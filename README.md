datalife_sale_repl_rs
=====================

The sale_repl_rs module of the Tryton application platform.

[![Build Status](https://drone.io/gitlab.com/datalifeit/trytond-sale_repl_rs/status.png)](https://drone.io/gitlab.com/datalifeit/trytond-sale_repl_rs/latest)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
