# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from tryton_replication import ReplLocalIdDestMixin

__all__ = ['Location']


class Location(ReplLocalIdDestMixin):
    __metaclass__ = PoolMeta
    __name__ = 'stock.location'
